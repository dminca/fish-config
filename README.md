# fish-config

This repo's directly sync'd with fish dir `~/.config/fish` therefore no
setup is necessary apart from `git push` and `git pull` to sync

Keep things simple

![ye](https://64.media.tumblr.com/69689040e29babacd5dc8a8409892987/08752591b7d86f97-04/s640x960/8556f81ae9eab3f09ce7d37eeda3cd8a2850d99a.jpg)

## Useful tips

* `kubectl` autocompletion? [Yes please][1]

[1]: https://kubernetes.io/docs/tasks/tools/included/optional-kubectl-configs-fish/
