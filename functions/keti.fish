function keti --wraps='kubectl exec -ti' --description 'alias keti=kubectl exec -ti'
  kubectl exec -ti $argv; 
end
