function gcs --wraps='git commit -s -v' --description 'alias gcs=git commit -s -v'
  git commit -s -v $argv
        
end
