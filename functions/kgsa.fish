function kgsa --wraps='kubectl get svc --all-namespaces' --wraps='kubectl get sa' --description 'alias kgsa=kubectl get sa'
  kubectl get sa $argv; 
end
