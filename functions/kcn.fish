function kcn --wraps='kubectl config set-context $(kubectl config current-context) --namespace' --description 'alias kcn=kubectl config set-context $(kubectl config current-context) --namespace'
  kubectl config set-context $(kubectl config current-context) --namespace $argv; 
end
