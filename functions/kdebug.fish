function kdebug --wraps='kubectl run sre-network-troubleshooter --rm -i --tty --image registry-proxy.td.gfk.com/nicolaka/netshoot -- /bin/bash' --description 'alias kdebug=kubectl run sre-network-troubleshooter --rm -i --tty --image registry-proxy.td.gfk.com/nicolaka/netshoot -- /bin/bash'
  kubectl run sre-network-troubleshooter --rm -i --tty --image registry-proxy.td.gfk.com/nicolaka/netshoot -- /bin/bash $argv; 
end
