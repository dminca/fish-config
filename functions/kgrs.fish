function kgrs --wraps='kubectl get rs' --description 'alias kgrs=kubectl get rs'
  kubectl get rs $argv; 
end
