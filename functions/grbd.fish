function grbd --wraps='git rebase develop' --description 'alias grbd=git rebase develop'
  git rebase develop $argv
        
end
