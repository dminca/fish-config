# This command is used a LOT both below and in daily life
alias -s k=kubectl

# Execute a kubectl command against all namespaces
alias -s kca='_kca(){ kubectl "$@" --all-namespaces;  unset -f _kca; }; _kca'

# Apply a YML file
alias -s kaf='kubectl apply -f'

# Drop into an interactive terminal on a container
alias -s keti='kubectl exec -ti'

# Manage configuration quickly to switch contexts between local, dev ad staging.
alias -s kcuc='kubectl config use-context'
alias -s kcsc='kubectl config set-context'
alias -s kcdc='kubectl config delete-context'
alias -s kccc='kubectl config current-context'

# List all contexts
alias -s kcgc='kubectl config get-contexts'

# General aliases
alias -s kdel='kubectl delete'
alias -s kdelf='kubectl delete -f'

# Pod management.
alias -s kgp='kubectl get pods'
alias -s kgpa='kubectl get pods --all-namespaces'
alias -s kgpw='kgp --watch'
alias -s kgpwide='kgp -o wide'
alias -s kep='kubectl edit pods'
alias -s kdp='kubectl describe pods'
alias -s kdelp='kubectl delete pods'

# get pod by label: kgpl "app=myapp" -n myns
alias -s kgpl='kgp -l'

# get pod by namespace: kgpn kube-system"
alias -s kgpn='kgp -n'

# Service management.
alias -s kgs='kubectl get svc'
alias -s kgsa='kubectl get svc --all-namespaces'
alias -s kgsw='kgs --watch'
alias -s kgswide='kgs -o wide'
alias -s kes='kubectl edit svc'
alias -s kds='kubectl describe svc'
alias -s kdels='kubectl delete svc'

# Ingress management
alias -s kgi='kubectl get ingress'
alias -s kgia='kubectl get ingress --all-namespaces'
alias -s kei='kubectl edit ingress'
alias -s kdi='kubectl describe ingress'
alias -s kdeli='kubectl delete ingress'

# Namespace management
alias -s kgns='kubectl get namespaces'
alias -s kens='kubectl edit namespace'
alias -s kdns='kubectl describe namespace'
alias -s kdelns='kubectl delete namespace'
alias -s kcn='kubectl config set-context $(kubectl config current-context) --namespace'

# ConfigMap management
alias -s kgcm='kubectl get configmaps'
alias -s kgcma='kubectl get configmaps --all-namespaces'
alias -s kecm='kubectl edit configmap'
alias -s kdcm='kubectl describe configmap'
alias -s kdelcm='kubectl delete configmap'

# Secret management
alias -s kgsec='kubectl get secret'
alias -s kgseca='kubectl get secret --all-namespaces'
alias -s kdsec='kubectl describe secret'
alias -s kdelsec='kubectl delete secret'

# Deployment management.
alias -s kgd='kubectl get deployment'
alias -s kgda='kubectl get deployment --all-namespaces'
alias -s kgdw='kgd --watch'
alias -s kgdwide='kgd -o wide'
alias -s ked='kubectl edit deployment'
alias -s kdd='kubectl describe deployment'
alias -s kdeld='kubectl delete deployment'
alias -s ksd='kubectl scale deployment'
alias -s krsd='kubectl rollout status deployment'

# Rollout management.
alias -s kgrs='kubectl get rs'
alias -s krh='kubectl rollout history'
alias -s kru='kubectl rollout undo'

# Statefulset management.
alias -s kgss='kubectl get statefulset'
alias -s kgssa='kubectl get statefulset --all-namespaces'
alias -s kgssw='kgss --watch'
alias -s kgsswide='kgss -o wide'
alias -s kess='kubectl edit statefulset'
alias -s kdss='kubectl describe statefulset'
alias -s kdelss='kubectl delete statefulset'
alias -s ksss='kubectl scale statefulset'
alias -s krsss='kubectl rollout status statefulset'

# Port forwarding
alias -s kpf="kubectl port-forward"

# Tools for accessing all information
alias -s kga='kubectl get all'
alias -s kgaa='kubectl get all --all-namespaces'

# Logs
alias -s kl='kubectl logs'
alias -s kl1h='kubectl logs --since 1h'
alias -s kl1m='kubectl logs --since 1m'
alias -s kl1s='kubectl logs --since 1s'
alias -s klf='kubectl logs -f'
alias -s klf1h='kubectl logs --since 1h -f'
alias -s klf1m='kubectl logs --since 1m -f'
alias -s klf1s='kubectl logs --since 1s -f'

# File copy
alias -s kcp='kubectl cp'

# Node Management
alias -s kgno='kubectl get nodes'
alias -s keno='kubectl edit node'
alias -s kdno='kubectl describe node'
alias -s kdelno='kubectl delete node'

# PVC management.
alias -s kgpvc='kubectl get pvc'
alias -s kgpvca='kubectl get pvc --all-namespaces'
alias -s kgpvcw='kgpvc --watch'
alias -s kepvc='kubectl edit pvc'
alias -s kdpvc='kubectl describe pvc'
alias -s kdelpvc='kubectl delete pvc'

# Service account management.
alias -s kgsa="kubectl get sa"
alias -s kdsa="kubectl describe sa"
alias -s kdelsa="kubectl delete sa"

# DaemonSet management.
alias -s kgds='kubectl get daemonset'
alias -s kgdsw='kgds --watch'
alias -s keds='kubectl edit daemonset'
alias -s kdds='kubectl describe daemonset'
alias -s kdelds='kubectl delete daemonset'

# CronJob management.
alias -s kgcj='kubectl get cronjob'
alias -s kecj='kubectl edit cronjob'
alias -s kdcj='kubectl describe cronjob'
alias -s kdelcj='kubectl delete cronjob'
