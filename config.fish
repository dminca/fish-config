if status is-interactive
    # Commands to run in interactive sessions can go here
    set --global --export SSH_AUTH_SOCK "$(gpgconf --list-dirs agent-ssh-socket)"
    set --global --export GPG_TTY "$(tty)"

    # https://github.com/ajeetdsouza/zoxide
    if type -q zoxide
      # TODO: perhaps suggest installing it if it's not present (prompt y/n)
      zoxide init fish | source
    end
end

if type -q nvim
    set --global --export EDITOR "nvim"
end

if type -q go
    set --global --export GOPATH "$HOME/Projects/misc/gopath"
    fish_add_path --global --append "$GOPATH/bin"
end

if type -q kubectl
    kubectl completion fish | source
end

# Disable the timestamp on the right side of the prompt
function fish_right_prompt
    #intentionally left blank
end

function bind_bang
    switch (commandline -t)[-1]
        case "!"
            commandline -t $history[1]; commandline -f repaint
        case "*"
            commandline -i !
    end
end

function bind_dollar
    switch (commandline -t)[-1]
        case "!"
            commandline -t ""
            commandline -f history-token-search-backward
        case "*"
            commandline -i '$'
    end
end

function fish_user_key_bindings
    bind ! bind_bang
    bind '$' bind_dollar
end
